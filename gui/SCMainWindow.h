#ifndef SCMAINWINDOW_H
#define SCMAINWINDOW_H

#include <QMainWindow>
#include <QMap>

class SCGraphicsController;
class SCScene;

namespace Ui {
    class SCMainWindow;
}

class SCMainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit SCMainWindow(QWidget *parent = 0);
    ~SCMainWindow();

public slots:
    //display dialogs
    void displayBackgroundColorChooserDialog();
    void displayBorderColorChooserDialog();
    void saveFileChooserDialog();
    void loadFileChooserDialog();

    //handle intputs
    void updateProperties(QMap<int, QString>);
    void getPropertiesInput();
    void rotateSelection();
    void updateSmallView(QRectF);

private:
    void setToolbarActions();
    void setMenuBarActions();
    void setLeftPanelActions();
    void setRightPanelActions();

    Ui::SCMainWindow *widget;
    SCGraphicsController *controller;
};

#endif // SCMAINWINDOW_H
