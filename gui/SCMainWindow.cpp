#include "SCMainWindow.h"
#include "ui_SCMainWindow.h"
#include "../app/SCGraphicsController.h"
#include "../app/SCScene.h"
#include "../app/items/SCItem.h"
#include <QColorDialog>
#include <QFileDialog>
#include <QMessageBox>


SCMainWindow::SCMainWindow(QWidget *parent) : QMainWindow(parent), widget(new Ui::SCMainWindow) {
    widget->setupUi(this);
    controller = new SCGraphicsController();
    widget->graphicsView->setScene(controller->getScene());
    widget->graphicsView->setDragMode(QGraphicsView::RubberBandDrag);
    widget->graphicsView->setBackgroundBrush(QBrush(QColor(204, 230, 181), Qt::CrossPattern));
    widget->smallViewWidget->setScene(controller->getScene());
    widget->smallView->setFixedSize(200, 200);
    connect(controller->getScene(), SIGNAL(sceneRectChanged(QRectF)), this, SLOT(updateSmallView(QRectF)));
    setToolbarActions();
    setMenuBarActions();
    setLeftPanelActions();
    setRightPanelActions();
}


SCMainWindow::~SCMainWindow() {
    if (widget) delete widget;
    if (controller) delete controller;
}


void SCMainWindow::displayBackgroundColorChooserDialog() {
    QColor color = QColorDialog::getColor();
    if (!color.isValid()) return;
    controller->setSelectionColor(color);
}


void SCMainWindow::displayBorderColorChooserDialog() {
    QColor color = QColorDialog::getColor();
    if (!color.isValid()) return;
    controller->setSelectionBorderColor(color);
}


void SCMainWindow::saveFileChooserDialog() {
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                    QDir::currentPath(),
                                                    tr("SCGRAPHICS Files (*.scg)"));
    if (fileName.isEmpty()) return;

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox box(this);
        box.setIcon(QMessageBox::Warning);
        box.setText("Cannot write to file " + fileName);
        box.exec();
        return;
    }
    file.close();

    controller->save(fileName);
}


void SCMainWindow::loadFileChooserDialog() {
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    QDir::currentPath(),
                                                    tr("SCGRAPHICS Files (*.scg)"));
    if (fileName.isEmpty()) return;

    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox box(this);
        box.setIcon(QMessageBox::Warning);
        box.setText("Cannot read file " + fileName);
        box.exec();
        return;
    }
    file.close();

    controller->load(fileName);
    widget->graphicsView->setScene(controller->getScene());
    widget->smallViewWidget->setScene(controller->getScene());
    updateSmallView(controller->getScene()->sceneRect());
    connect(controller->getScene(), SIGNAL(sceneRectChanged(QRectF)), this, SLOT(updateSmallView(QRectF)));
}


void SCMainWindow::updateProperties(QMap<int, QString> data) {
    widget->objectNameInput->setText(data.value(Name));
    widget->alphaAmount->setSliderPosition(data.value(Alpha).toInt());
    widget->borderSize->setValue(data.value(BorderSize).toFloat());
}


void SCMainWindow::getPropertiesInput() {
    QMap<int, QString> data;
    data.insert(Name, widget->objectNameInput->text());
    data.insert(Alpha, QString::number(widget->alphaAmount->sliderPosition()));
    data.insert(BorderSize, QString::number(widget->borderSize->value()));
    controller->setSelectionData(data);
}


void SCMainWindow::rotateSelection() {
    QString angleStr = widget->rotateInput->text();
    bool convertionSuccess = false;
    qreal angle = angleStr.toFloat(&convertionSuccess);
    if (convertionSuccess) {
        angle = widget->rotateLeft->isChecked() ? - angle : angle;
        controller->rotateSelection(angle);
    } else {
        QMessageBox box(this);
        box.setText("Rotation angle is not valid.");
        box.exec();
    }
}


void SCMainWindow::updateSmallView(QRectF sceneRectangle) {
    qreal scale = std::min((widget->smallViewWidget->width() - 10) / sceneRectangle.width(), (widget->smallViewWidget->height() - 10) / sceneRectangle.height());
    QTransform transform = QTransform::fromScale(scale, scale);
    widget->smallViewWidget->setTransform(transform);
}


void SCMainWindow::setToolbarActions() {
    QAction *insertLine = widget->mainToolBar->addAction(QIcon(":/resource/icons/addline.png"), "Line");
    QAction *insertRectangle = widget->mainToolBar->addAction(QIcon(":/resource/icons/addrect.png"), "Rectangle");
    QAction *insertEllipse = widget->mainToolBar->addAction(QIcon(":/resource/icons/addellipse.png"), "Ellipse");
    QAction *chooseBorderColor = widget->toolBar->addAction(QIcon(":/resource/icons/bordercolor.png"), "Border Color");
    QAction *chooseBackgroundColor = widget->toolBar->addAction(QIcon(":/resource/icons/backgroundcolor.png"), "Background Color");
    QAction *sendToBack = widget->toolBar->addAction(QIcon(":/resource/icons/sendtoback.png"), "Send To Back");
    QAction *bringToFront = widget->toolBar->addAction(QIcon(":/resource/icons/bringtofront.png"), "Bring To Front");
    QAction *group = widget->toolBar->addAction(QIcon(":/resource/icons/addgroup.png"), "Group");
    QAction *unGroup = widget->toolBar->addAction(QIcon(":/resource/icons/ungroup.png"), "UnGroup");
    QAction *remove = widget->toolBar->addAction(QIcon(":/resource/icons/delete.png"), "Delete");

    connect(insertRectangle, SIGNAL(triggered()), controller, SLOT(insertRectangle()));
    connect(chooseBackgroundColor, SIGNAL(triggered()), this, SLOT(displayBackgroundColorChooserDialog()));
    connect(chooseBorderColor, SIGNAL(triggered()), this, SLOT(displayBorderColorChooserDialog()));
    connect(sendToBack, SIGNAL(triggered()), controller, SLOT(sendSelectionToBack()));
    connect(bringToFront, SIGNAL(triggered()), controller, SLOT(bringSelectionToFront()));
    connect(insertEllipse, SIGNAL(triggered()), controller, SLOT(insertEllipse()));
    connect(insertLine, SIGNAL(triggered()), controller, SLOT(insertLine()));
    connect(group, SIGNAL(triggered()), controller, SLOT(insertGroup()));
    connect(unGroup, SIGNAL(triggered()), controller, SLOT(removeGroup()));
    connect(remove, SIGNAL(triggered()), controller, SLOT(removeSelection()));
}


void SCMainWindow::setMenuBarActions() {
    connect(widget->actionSave, SIGNAL(triggered()), this, SLOT(saveFileChooserDialog()));
    connect(widget->actionOpen, SIGNAL(triggered()), this, SLOT(loadFileChooserDialog()));
    connect(widget->actionCopy, SIGNAL(triggered()), controller, SLOT(copySelection()));
    connect(widget->actionPaste, SIGNAL(triggered()), controller, SLOT(pasteSelection()));
}


void SCMainWindow::setLeftPanelActions() {
    connect(widget->rotateButton, SIGNAL(pressed()), this, SLOT(rotateSelection()));
}


void SCMainWindow::setRightPanelActions() {
    connect(widget->objectNameInput, SIGNAL(editingFinished()), this, SLOT(getPropertiesInput()));
    connect(widget->alphaAmount, SIGNAL(sliderReleased()), this, SLOT(getPropertiesInput()));
    connect(widget->borderSize, SIGNAL(valueChanged(double)), this, SLOT(getPropertiesInput()));
    connect(controller, SIGNAL(selectionDataChanged(QMap<int,QString>)), this, SLOT(updateProperties(QMap<int, QString>)));
}
