#-------------------------------------------------
#
# Project created by QtCreator 2011-10-07T22:38:03
#
#-------------------------------------------------

QT       += core gui

TARGET = SCGraphics
TEMPLATE = app


SOURCES += \
    main.cpp \
    gui/SCMainWindow.cpp \
    app/SCSceneSaver.cpp \
    app/SCSceneLoader.cpp \
    app/SCScene.cpp \
    app/SCGraphicsController.cpp \
    app/IO.cpp \
    app/items/SCResizableItem.cpp \
    app/items/SCRectangleItem.cpp \
    app/items/SCLineItem.cpp \
    app/items/SCItemGroup.cpp \
    app/items/SCItem.cpp \
    app/items/SCExportable.cpp \
    app/items/SCEllipseItem.cpp

HEADERS  += \
    gui/SCMainWindow.h \
    app/SCSceneSaver.h \
    app/SCSceneLoader.h \
    app/SCScene.h \
    app/SCGraphicsController.h \
    app/IO.h \
    app/items/SCResizableItem.h \
    app/items/SCRectangleItem.h \
    app/items/SCLineItem.h \
    app/items/SCItemGroup.h \
    app/items/SCItem.h \
    app/items/SCImportable.h \
    app/items/SCExportable.h \
    app/items/SCEllipseItem.h \
    app/items/SCClonable.h

FORMS    += \
    gui/SCMainWindow.ui

RESOURCES += \
    gui/icons.qrc

OTHER_FILES +=





















































































































































