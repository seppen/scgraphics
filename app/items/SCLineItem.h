#ifndef SCLINEITEM_H
#define SCLINEITEM_H

#include "SCItem.h"
#include "SCExportable.h"
#include "SCClonable.h"
#include "SCImportable.h"
#include <QGraphicsLineItem>

class SCLineItem : public QGraphicsLineItem, public SCItem, public SCExportable, public SCClonable, public SCImportable {

public:

    //SCItem
    void resizeToRectangle(QPointF, QPointF);
    void setPen(QPen);
    QPen getPen() const;

    //SCClonable
    SCLineItem* clone();

    //SCImportable
    static SCLineItem* import(QXmlStreamReader*, QMap<qreal, QGraphicsItem*>);

    enum {Type = UserType + 5};

    int type() const {
        return Type;
    }

    static const QString tag() {
        return "line";
    }

protected:

    //SCExportable
    void exportHelper(QXmlStreamWriter *, qreal);
    QString tagName();
};

#endif // SCLINEITEM_H
