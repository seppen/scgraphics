#ifndef SCIMPORTABLE_H
#define SCIMPORTABLE_H

#include <QXmlStreamReader>
#include <QMap>
class QGraphicsItem;

class SCImportable {

public:
    static SCImportable* import(QXmlStreamReader*, QMap<qreal, QGraphicsItem*>);
};

#endif // SCIMPORTABLE_H
