#ifndef SCRESIZABLEITEM_H
#define SCRESIZABLEITEM_H

#include <QGraphicsItem>
#include "SCItem.h"
#include "SCClonable.h"
#include "SCExportable.h"
#include "SCImportable.h"

class SCResizableItem : public QGraphicsItem, public SCItem, public SCClonable, public SCExportable, public SCImportable {

public:
    SCResizableItem(SCItem*);
    ~SCResizableItem();

    //QGraphicsItem
    void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*);
    bool contains(const QPointF&) const;
    QRectF boundingRect() const;

    //SCItem
    void resizeToRectangle(QPointF, QPointF);
    void setAreaBrush(QBrush);
    QBrush getAreaBrush() const;
    void setPen(QPen);
    QPen getPen() const;

    //SCClonable
    SCResizableItem* clone();

    //SCImportable
    static SCResizableItem* import(QXmlStreamReader *, QMap<qreal, QGraphicsItem *>);

    QGraphicsItem* getItem();
    void reparent();
    void rotateItem(qreal);
    void setTransformToInner(const QTransform&, bool);

    enum {Type = UserType + 1};

    int type() const {
        return Type;
    }

    static const QString tag() {
        return "item";
    }

protected:

    //SCExportable
    void exportHelper(QXmlStreamWriter *, qreal);
    QString tagName();

    enum Handle {None = -1, TopLeftCorner, Top, TopRightCorner, Left, Right, BottomLeftCorner, Bottom, BottomRightCorner, Rotate};
    //squares
    void initSquares();
    void positionSquares();
    void setSquaresVisibility(bool);

    //rotate circle
    void initRotateCircle();
    void positionRotateCircle();
    void setRotateCircleVisibility(bool);

    //events
    void mousePressEvent(QGraphicsSceneMouseEvent*);
    void mouseMoveEvent(QGraphicsSceneMouseEvent*);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent*);

private:
    const int SQUARE_SIZE;
    const int SQUARE_COUNT;
    QGraphicsRectItem **squares;
    QGraphicsEllipseItem *rotateCircle;
    Handle activeHandle;
    SCItem *innerItem;
};

#endif // SCRESIZABLEITEM_H
