#ifndef SCITEMGROUP_H
#define SCITEMGROUP_H

#include <QGraphicsItemGroup>
#include "SCClonable.h"
#include "SCItem.h"
#include "SCExportable.h"
#include "SCImportable.h"

class SCItemGroup : public QGraphicsItemGroup, public SCItem, public SCExportable, public SCClonable, public SCImportable {

public:

    //SCItem
    void resizeToRectangle(QPointF, QPointF);

    //SCClonable
    SCItemGroup* clone();

    //SCImportable
    static SCItemGroup* import(QXmlStreamReader*, QMap<qreal, QGraphicsItem*>);

    enum {Type = UserType + 3};

    int type() const {
        return Type;
    }

    static const QString tag() {
        return "group";
    }

protected:

    //SCExportable
    void exportHelper(QXmlStreamWriter *, qreal);
    QString tagName();
};

#endif // SCITEMGROUP_H
