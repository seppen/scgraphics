#include "SCRectangleItem.h"
#include "../IO.h"
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <cmath>
#include <QPen>
#include <QBrush>


SCRectangleItem::SCRectangleItem() : QGraphicsRectItem(0, 0, 1, 1) {
    setBrush(QBrush(QColor(0, 0, 0, 0)));
}


void SCRectangleItem::resizeToRectangle(QPointF start, QPointF end) {
    QPointF top = QPointF(
                std::min(start.x(), end.x()),
                std::min(start.y(), end.y())
    );
    QRectF rect = QRectF(
               top - top,
               QPointF(
                    std::max(start.x(), end.x()),
                    std::max(start.y(), end.y())
                ) - top
            );
    setRect(rect);
}


void SCRectangleItem::setAreaBrush(QBrush brush) {
    QAbstractGraphicsShapeItem::setBrush(brush);
}


QBrush SCRectangleItem::getAreaBrush() const {
    return QAbstractGraphicsShapeItem::brush();
}


void SCRectangleItem::setPen(QPen pen) {
    QAbstractGraphicsShapeItem::setPen(pen);
}


QPen SCRectangleItem::getPen() const {
    return QAbstractGraphicsShapeItem::pen();
}


SCRectangleItem* SCRectangleItem::clone() {
    SCRectangleItem *rectangle = new SCRectangleItem();
    rectangle->setRect(rect());
    rectangle->setTransform(transform());
    rectangle->setBrush(brush());
    rectangle->setPen(pen());
    return rectangle;
}


SCRectangleItem* SCRectangleItem::import(QXmlStreamReader *xml, QMap<qreal, QGraphicsItem *> imported) {
    SCRectangleItem *rectangle = new SCRectangleItem();
    rectangle->setAreaBrush(QBrush(IO::readNextColor(xml)));
    rectangle->setPen(IO::readNextPen(xml));
    QSizeF size = IO::readNextSize(xml);
    rectangle->setRect(0, 0, size.width(), size.height());
    rectangle->setTransform(IO::readNextTransform(xml));
    return rectangle;
}


void SCRectangleItem::exportHelper(QXmlStreamWriter *xml, qreal id) {
    IO::writeColor(xml, brush().color());
    IO::writePen(xml, pen());
    IO::writeSize(xml, rect().size());
    IO::writeTransform(xml, transform());
}


QString SCRectangleItem::tagName() {
    return tag();
}
