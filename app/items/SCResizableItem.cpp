#include "SCResizableItem.h"
#include "SCEllipseItem.h"
#include "SCLineItem.h"
#include "SCRectangleItem.h"
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QTextItem>
#include "../IO.h"
#include <QXmlStreamWriter>
#include <QStyleOptionGraphicsItem>
#include "SCItemGroup.h"
#include <cmath>


SCResizableItem::SCResizableItem(SCItem *innerItem) : SQUARE_SIZE(5), SQUARE_COUNT(8) {
    this->innerItem = innerItem;
    setFlags(QGraphicsItem::ItemIsSelectable|QGraphicsItem::ItemIsMovable);
    reparent();
    initSquares();
    initRotateCircle();
    positionSquares();
    positionRotateCircle();
    setSquaresVisibility(false);
}


SCResizableItem::~SCResizableItem() {
    if (innerItem) delete dynamic_cast<QGraphicsItem*>(innerItem);
    for (int i = 0; i < SQUARE_COUNT; i++)
        delete squares[i];
    delete squares;
}


void SCResizableItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    if (!isSelected()) {
        setSquaresVisibility(false);
    } else {
        setSquaresVisibility(true);
        painter->drawRect(boundingRect());
    }
}


bool SCResizableItem::contains(const QPointF &point) const {
    return dynamic_cast<QGraphicsItem*>(innerItem)->contains(point);
}


QRectF SCResizableItem::boundingRect() const {
    QRectF ib = dynamic_cast<QGraphicsItem*>(innerItem)->boundingRect();
    QGraphicsItem *it = dynamic_cast<QGraphicsItem*>(innerItem);
    qreal minX = 0;
    qreal maxX = 0;
    qreal minY = 0;
    qreal maxY = 0;

    QPointF topLeft = it->transform().map(ib.topLeft());
    QPointF topRight = it->transform().map(ib.topRight());
    QPointF bottomRight = it->transform().map(ib.bottomRight());
    QPointF bottomLeft = it->transform().map(ib.bottomLeft());

    minX = std::min(topLeft.x(), topRight.x());
    minX = std::min(bottomLeft.x(), minX);
    minX = std::min(bottomRight.x(), minX);

    maxX = std::max(topLeft.x(), topRight.x());
    maxX = std::max(bottomLeft.x(), maxX);
    maxX = std::max(bottomRight.x(), maxX);

    minY = std::min(topLeft.y(), topRight.y());
    minY = std::min(bottomLeft.y(), minY);
    minY = std::min(bottomRight.y(), minY);

    maxY = std::max(topLeft.y(), topRight.y());
    maxY = std::max(bottomLeft.y(), maxY);
    maxY = std::max(bottomRight.y(), maxY);

    return QRectF(minX, minY, maxX - minX, maxY - minY);
}


void SCResizableItem::resizeToRectangle(QPointF start, QPointF end) {
    innerItem->resizeToRectangle(start, end);
    positionSquares();
}


void SCResizableItem::setAreaBrush(QBrush brush) {
    innerItem->setAreaBrush(brush);
}


QBrush SCResizableItem::getAreaBrush() const {
    return innerItem->getAreaBrush();
}


void SCResizableItem::setPen(QPen pen) {
    innerItem->setPen(pen);
}


QPen SCResizableItem::getPen() const {
    return innerItem->getPen();
}


SCResizableItem* SCResizableItem::clone() {
    SCClonable *cloned = dynamic_cast<SCClonable*>(innerItem)->clone();
    SCResizableItem *ri = new SCResizableItem(dynamic_cast<SCItem*>(cloned));
    ri->setPos(scenePos());
    return ri;
}


SCResizableItem* SCResizableItem::import(QXmlStreamReader *xml, QMap<qreal, QGraphicsItem *> imported) {
    qreal z = xml->attributes().value("z").toString().toFloat();
    qreal id = xml->attributes().value("id").toString().toFloat();
    QString name = xml->attributes().value("name").toString();
    QPointF pos = IO::readNextPos(xml);

    xml->readNext();
    while (!xml->isStartElement() && !xml->atEnd()) xml->readNext();

    SCItem *item;
    if (SCRectangleItem::tag() == xml->name().toString()) {
        item = SCRectangleItem::import(xml, imported);
    } else if (SCEllipseItem::tag() == xml->name().toString()) {
        item = SCEllipseItem::import(xml, imported);
    } else if (SCLineItem::tag() == xml->name().toString()) {
        item = SCLineItem::import(xml, imported);
    } else if (SCItemGroup::tag() == xml->name().toString()) {
        item = SCItemGroup::import(xml, imported);
    }
    xml->readNext();
    SCResizableItem *ri = new SCResizableItem(item);
    ri->setPos(pos);
    ri->setZValue(z);
    ri->setId(id);
    ri->setData(Name, name);

    return ri;
}


QGraphicsItem* SCResizableItem::getItem() {
    return dynamic_cast<QGraphicsItem*>(innerItem);
}


void SCResizableItem::reparent() {
    getItem()->setParentItem(this);
}


void SCResizableItem::rotateItem(qreal angle) {
    QGraphicsItem *it = getItem();
    QPointF top = boundingRect().center();
    QTransform r;
    r.translate(top.x(), top.y());
    r.rotate(angle);
    r.translate(- top.x(), - top.y());
    it->setTransform(it->transform() * r);
    positionSquares();
}


void SCResizableItem::setTransformToInner(const QTransform &matrix, bool combine) {
    if (true == combine) {
        getItem()->setTransform(getItem()->transform() * matrix);
    } else {
        getItem()->setTransform(matrix);
    }
}


void SCResizableItem::exportHelper(QXmlStreamWriter *xml, qreal id) {
    setId(id);
    xml->writeAttribute("id", QString::number(getId()));
    xml->writeAttribute("z", QString::number(zValue()));
    xml->writeAttribute("name", data(Name).toString());
    IO::writePos(xml, pos());
    dynamic_cast<SCExportable*>(getItem())->exportt(xml, 0);
}


QString SCResizableItem::tagName() {
    return tag();
}


void SCResizableItem::initSquares() {
    squares = new QGraphicsRectItem*[SQUARE_COUNT];
    for (int i = 0; i < SQUARE_COUNT; i++) {
        squares[i] = new QGraphicsRectItem(this);
        squares[i]->setFlag(QGraphicsItem::ItemIsSelectable, false);
        squares[i]->setFlag(QGraphicsItem::ItemIgnoresTransformations, true);
        squares[i]->setRect(0, 0, SQUARE_SIZE, SQUARE_SIZE);
        dynamic_cast<QAbstractGraphicsShapeItem*>(squares[i])->setBrush(QBrush(QColor(85, 190, 73)));
        dynamic_cast<QAbstractGraphicsShapeItem*>(squares[i])->setPen(QPen(QBrush(), 0));
    }
}


void SCResizableItem::positionSquares() {
    QRectF bounding = boundingRect();
    QPointF top = bounding.topLeft();
    squares[TopLeftCorner]->setPos(top);
    squares[Top]->setPos((bounding.width() - SQUARE_SIZE) / 2 + top.x(), top.y());
    squares[TopRightCorner]->setPos(bounding.width() - SQUARE_SIZE + top.x(), top.y());
    squares[Left]->setPos(top.x(), (bounding.height() - SQUARE_SIZE) / 2 + top.y());
    squares[Right]->setPos(bounding.width() - SQUARE_SIZE + top.x(), (bounding.height() - SQUARE_SIZE) / 2 + top.y());
    squares[BottomLeftCorner]->setPos(top.x(), bounding.height() - SQUARE_SIZE + top.y());
    squares[Bottom]->setPos((bounding.width() - SQUARE_SIZE) / 2 + top.x(), bounding.height() - SQUARE_SIZE + top.y());
    squares[BottomRightCorner]->setPos(bounding.width() - SQUARE_SIZE + top.x(), bounding.height() - SQUARE_SIZE + top.y());
}


void SCResizableItem::setSquaresVisibility(bool visible) {
    for (int i = 0; i < SQUARE_COUNT; i++) {
        squares[i]->setVisible(visible);
    }
}


void SCResizableItem::initRotateCircle() {

}


void SCResizableItem::positionRotateCircle() {

}


void SCResizableItem::setRotateCircleVisibility(bool visible) {

}


void SCResizableItem::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    for (int i = TopLeftCorner; i <= BottomRightCorner; i++) {
        if (squares[i]->contains(squares[i]->mapFromScene(event->scenePos()))) {
            activeHandle = (Handle)i;
        }
    }
    QGraphicsItem::mousePressEvent(event);
}


void SCResizableItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    QGraphicsItem *it = getItem();
    QRectF bounding = boundingRect();
    QPointF top = bounding.topLeft();
    qreal dx = event->lastPos().x() - event->pos().x();
    qreal dy = event->lastPos().y() - event->pos().y();
    switch (activeHandle) {
    case Left:
        translate(- top.x(), - top.y());
        scale(1 + dx / bounding.width(), 1);
        translate(top.x() - dx, top.y());
        it->setTransform(it->transform() * transform());
        setTransform(QTransform());
        break;
    case Right:
        translate(- top.x(), - top.y());
        scale(1 - dx / bounding.width(), 1);
        translate(top.x(), top.y());
        it->setTransform(it->transform() * transform());
        setTransform(QTransform());
        break;
    case Bottom:
        translate(- top.x(), - top.y());
        scale(1, 1 - dy / bounding.height());
        translate(top.x(), top.y());
        it->setTransform(it->transform() * transform());
        setTransform(QTransform());
        break;
    case Top:
        translate(- top.x(), - top.y());
        scale(1, 1 + dy / bounding.height());
        translate(top.x(), top.y() - dy);
        it->setTransform(it->transform() * transform());
        setTransform(QTransform());
        break;
    default:
        QGraphicsItem::mouseMoveEvent(event);
    }
    positionSquares();
}


void SCResizableItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    activeHandle = None;
    QGraphicsItem::mouseReleaseEvent(event);
}
