#include "SCExportable.h"
#include <QXmlStreamWriter>


void SCExportable::exportt(QXmlStreamWriter *xml, qreal id) {
    xml->writeStartElement(tagName());
    exportHelper(xml, id);
    xml->writeEndElement();
}


qreal SCExportable::getId() {
    return id;
}


void SCExportable::setId(qreal id) {
    this->id = id;
}
