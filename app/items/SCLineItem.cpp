#include "SCLineItem.h"
#include <QPen>
#include <QXmlStreamWriter>
#include "../IO.h"


void SCLineItem::resizeToRectangle(QPointF start, QPointF end) {
    start = mapFromScene(start);
    end = mapFromScene(end);
    setLine(start.x(), start.y(), end.x(), end.y());
}


void SCLineItem::setPen(QPen pen) {
    QGraphicsLineItem::setPen(pen);
}


QPen SCLineItem::getPen() const {
    return pen();
}


SCLineItem* SCLineItem::clone() {
    SCLineItem *cloned = new SCLineItem();
    cloned->setLine(line());
    cloned->setPen(pen());
    cloned->setTransform(transform());
    return cloned;
}


SCLineItem* SCLineItem::import(QXmlStreamReader *xml, QMap<qreal, QGraphicsItem *> imported) {
    SCLineItem *lineItem = new SCLineItem();
    lineItem->setPen(IO::readNextPen(xml));
    IO::jumpToNextElement(xml, "start");
    qreal px = xml->attributes().value("x").toString().toFloat();
    qreal py = xml->attributes().value("y").toString().toFloat();
    IO::jumpToNextElement(xml, "end");
    qreal qx = xml->attributes().value("x").toString().toFloat();
    qreal qy = xml->attributes().value("y").toString().toFloat();
    lineItem->setLine(px, py, qx, qy);
    lineItem->setTransform(IO::readNextTransform(xml));
    return lineItem;
}


void SCLineItem::exportHelper(QXmlStreamWriter *xml, qreal id) {
    IO::writePen(xml, getPen());
    xml->writeStartElement("start");
    xml->writeAttribute("x", QString::number(line().x1()));
    xml->writeAttribute("y", QString::number(line().y1()));
    xml->writeEndElement();
    xml->writeStartElement("end");
    xml->writeAttribute("x", QString::number(line().x2()));
    xml->writeAttribute("y", QString::number(line().y2()));
    xml->writeEndElement();
    IO::writeTransform(xml, transform());
}


QString SCLineItem::tagName() {
    return tag();
}
