#ifndef SCCLONABLE_H
#define SCCLONABLE_H

class SCClonable {

public:
    virtual SCClonable* clone() = 0;
};

#endif // SCCLONABLE_H
