#include "SCEllipseItem.h"
#include <QXmlStreamwriter>
#include "../IO.h"
#include <QPen>
#include <QBrush>


SCEllipseItem::SCEllipseItem() : QGraphicsEllipseItem(0, 0, 1, 1) {
    setBrush(QBrush(QColor(0, 0, 0, 0)));
}


void SCEllipseItem::resizeToRectangle(QPointF start, QPointF end) {
    QPointF top = QPointF(
                std::min(start.x(), end.x()),
                std::min(start.y(), end.y())
    );
    QRectF rect = QRectF(
               top - top,
               QPointF(
                    std::max(start.x(), end.x()),
                    std::max(start.y(), end.y())
                ) - top
            );
    setRect(rect);
}


void SCEllipseItem::setAreaBrush(QBrush brush) {
    QAbstractGraphicsShapeItem::setBrush(brush);
}


QBrush SCEllipseItem::getAreaBrush() const {
    return QAbstractGraphicsShapeItem::brush();
}


void SCEllipseItem::setPen(QPen pen) {
    QAbstractGraphicsShapeItem::setPen(pen);
}


QPen SCEllipseItem::getPen() const {
    return QAbstractGraphicsShapeItem::pen();
}


SCEllipseItem* SCEllipseItem::clone() {
    SCEllipseItem *ellipse = new SCEllipseItem();
    ellipse->setRect(rect());
    ellipse->setTransform(transform());
    ellipse->setBrush(brush());
    ellipse->setPen(pen());
    return ellipse;
}


SCEllipseItem* SCEllipseItem::import(QXmlStreamReader *xml, QMap<qreal, QGraphicsItem*> imported) {
    SCEllipseItem *ellipse = new SCEllipseItem();

    ellipse->setAreaBrush(QBrush(IO::readNextColor(xml)));
    ellipse->setPen(IO::readNextPen(xml));
    QSizeF size = IO::readNextSize(xml);
    ellipse->setRect(0, 0, size.width(), size.height());
    ellipse->setTransform(IO::readNextTransform(xml));

    return ellipse;
}


void SCEllipseItem::exportHelper(QXmlStreamWriter *xml, qreal id) {
    IO::writeColor(xml, brush().color());
    IO::writePen(xml, pen());
    IO::writeSize(xml, rect().size());
    IO::writeTransform(xml, transform());
}


QString SCEllipseItem::tagName() {
    return tag();
}
