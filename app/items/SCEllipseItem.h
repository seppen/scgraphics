#ifndef SCELLIPSEITEM_H
#define SCELLIPSEITEM_H

#include <QGraphicsEllipseItem>
#include "SCItem.h"
#include "SCExportable.h"
#include "SCImportable.h"
#include "SCClonable.h"

class SCEllipseItem : public QGraphicsEllipseItem, public SCItem, public SCExportable, public SCClonable, public SCImportable {

public:
    SCEllipseItem();

    //SCItem
    void resizeToRectangle(QPointF, QPointF);
    void setAreaBrush(QBrush);
    QBrush getAreaBrush() const;
    void setPen(QPen);
    QPen getPen() const;

    //SCClonable
    SCEllipseItem* clone();

    //SCImportable
    static SCEllipseItem* import(QXmlStreamReader*, QMap<qreal, QGraphicsItem*>);

    enum {Type = UserType + 4};

    int type() const {
        return Type;
    }

    static const QString tag() {
        return "ellipse";
    }

protected:

    //SCExportable
    void exportHelper(QXmlStreamWriter *, qreal);
    QString tagName();
};

#endif // SCELLIPSEITEM_H
