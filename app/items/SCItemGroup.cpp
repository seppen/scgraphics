#include "SCItemGroup.h"
#include "../IO.h"
#include "SCResizableItem.h"
#include <QBrush>
#include <QXmlStreamWriter>
#include <cmath>


/**
  @todo implement
*/
void SCItemGroup::resizeToRectangle(QPointF start, QPointF end) {
    scale(1, 1);
}


SCItemGroup* SCItemGroup::clone() {
    SCItemGroup* itemGroup = new SCItemGroup();
    QList<QGraphicsItem*> it = children();
    foreach (QGraphicsItem* outerItem, it) {
        if (dynamic_cast<SCClonable*>(outerItem)) {
            SCClonable *outerItemClonable = dynamic_cast<SCClonable*>(outerItem);
            itemGroup->addToGroup(dynamic_cast<QGraphicsItem*>(outerItemClonable->clone()));
        }
    }
    return itemGroup;
}


SCItemGroup* SCItemGroup::import(QXmlStreamReader *xml, QMap<qreal, QGraphicsItem*> imported) {
    SCItemGroup *itemGroup = new SCItemGroup();

    while (!("transform" == xml->name().toString() && xml->isStartElement())) {
        if ("itemRef" == xml->name().toString() && xml->isStartElement()) {
            QGraphicsItem *outerItem = imported.value(xml->attributes().value("id").toString().toFloat());
            itemGroup->addToGroup(imported.value(xml->attributes().value("id").toString().toFloat()));
        }
        xml->readNext();
    }
    itemGroup->setTransform(IO::readNextTransform(xml));

    return itemGroup;
}


void SCItemGroup::exportHelper(QXmlStreamWriter *xml, qreal id) {
    QList<QGraphicsItem*> it = children();

    foreach (QGraphicsItem *outerItem, it) {
        if (SCResizableItem::Type == outerItem->type()) {
            qreal itemId = dynamic_cast<SCResizableItem*>(outerItem)->getId();
            xml->writeStartElement("itemRef");
            xml->writeAttribute("id", QString::number(itemId));
            xml->writeEndElement();
        }
    }
    IO::writeTransform(xml, transform());
}


QString SCItemGroup::tagName() {
    return tag();
}
