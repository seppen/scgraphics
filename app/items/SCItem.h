#ifndef SCITEM_H
#define SCITEM_H

#include <QAbstractGraphicsShapeItem>

class SCItem {

public:
    virtual void resizeToRectangle(QPointF, QPointF) = 0;
    virtual void setAreaBrush(QBrush);
    virtual QBrush getAreaBrush() const;
    virtual void setPen(QPen);
    virtual QPen getPen() const;
    virtual ~SCItem();
};

enum ItemProperties {Order = 0, Name, Alpha, BorderSize};

#endif // SCITEM_H
