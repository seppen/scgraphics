#include "SCItem.h"
#include <QAbstractGraphicsShapeItem>
#include <QBrush>
#include <QPen>


void SCItem::setAreaBrush(QBrush brush) {
    return;
}


QBrush SCItem::getAreaBrush() const {
    return QBrush();
}


void SCItem::setPen(QPen pen) {
    return;
}


QPen SCItem::getPen() const {
    return QPen();
}


SCItem::~SCItem() {

}
