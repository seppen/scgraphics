#ifndef SCRECTANGLEITEM_H
#define SCRECTANGLEITEM_H

#include <QGraphicsRectItem>
#include "SCItem.h"
#include "SCExportable.h"
#include "SCClonable.h"
#include "SCImportable.h"

class SCRectangleItem : public QGraphicsRectItem, public SCItem, public SCExportable, public SCClonable, public SCImportable {

public:
    SCRectangleItem();

    //SCItem
    void resizeToRectangle(QPointF, QPointF);
    void setAreaBrush(QBrush);
    QBrush getAreaBrush() const;
    void setPen(QPen);
    QPen getPen() const;

    //SCClonable
    SCRectangleItem* clone();

    //SCImportable
    static SCRectangleItem* import(QXmlStreamReader*, QMap<qreal, QGraphicsItem*>);

    enum {Type = UserType + 2};

    int type() const {
        return Type;
    }

    static const QString tag() {
        return "rectangle";
    }

protected:

    //SCExportable
    void exportHelper(QXmlStreamWriter *, qreal);
    QString tagName();

private:
};

#endif // SCRECTANGLEITEM_H
