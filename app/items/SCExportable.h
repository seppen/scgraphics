#ifndef SCEXPORTABLE_H
#define SCEXPORTABLE_H

#include <QColor>

class QXmlStreamWriter;
class QString;

class SCExportable {

public:
    void exportt(QXmlStreamWriter*, qreal);
    void setId(qreal);
    qreal getId();
    static const QString tag();


protected:
    virtual void exportHelper(QXmlStreamWriter*, qreal) = 0;
    virtual QString tagName() = 0;

private:
    qreal id;

};

#endif // SCEXPORTABLE_H
