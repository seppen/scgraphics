#ifndef IO_H
#define IO_H

class QColor;
class QXmlStreamWriter;
class QPointF;
class QSizeF;
class QTransform;
class QXmlStreamReader;
class QString;
class QPen;

class IO {

public:
    IO();
    static void writePen(QXmlStreamWriter*, QPen);
    static void writeColor(QXmlStreamWriter*, QColor);
    static void writePos(QXmlStreamWriter*, QPointF);
    static void writeSize(QXmlStreamWriter*, QSizeF);
    static void writeTransform(QXmlStreamWriter*, QTransform);
    static QPen readNextPen(QXmlStreamReader*);
    static QColor readNextColor(QXmlStreamReader*);
    static QPointF readNextPos(QXmlStreamReader*);
    static QSizeF readNextSize(QXmlStreamReader*);
    static QTransform readNextTransform(QXmlStreamReader*);
    static void jumpToNextElement(QXmlStreamReader*, QString);
};

#endif // IO_H
