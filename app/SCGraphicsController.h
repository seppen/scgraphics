#ifndef SCGRAPHICSCONTROLLER_H
#define SCGRAPHICSCONTROLLER_H

#include <QObject>
#include "SCScene.h"
#include <QGraphicsRectItem>
#include <QList>

class SCGraphicsController : public QObject {
    Q_OBJECT

public:
    SCGraphicsController();
    ~SCGraphicsController();
    SCScene* getScene();
    void save(QString);
    void load(QString);

public slots:
    void setSelectionColor(QColor);
    void setSelectionBorderColor(QColor);

    //insertions
    void insertRectangle();
    void insertEllipse();
    void insertMultiItem();
    void insertGroup();
    void insertLine();

    void sendSelectionToBack();
    void bringSelectionToFront();
    void rotateSelection(qreal);

    void setSelectionData(QMap<int, QString>);
    void getSelectionData();

    void copySelection();
    void pasteSelection();
    void removeSelection();

    void removeGroup();

signals:
    void selectionDataChanged(QMap<int, QString>);

protected:
    void connectWithScene();
    void setSceneMode(SCScene::SceneMode);
    bool isInsertedItemAddedToScene();
    bool isOnlyOneTopItemInSelection();
    void insertItem(SCResizableItem*);

private:
    QColor color;
    SCResizableItem *insertedItem;
    SCScene *scene;
    QList<QGraphicsItem*> copied;
};

#endif // SCGRAPHICSCONTROLLER_H
