#include "SCGraphicsController.h"
#include "SCScene.h"
#include "items/SCLineItem.h"
#include "items/SCRectangleItem.h"
#include "items/SCEllipseItem.h"
#include "items/SCResizableItem.h"
#include "items/SCItemGroup.h"
#include "SCSceneSaver.h"
#include "SCSceneLoader.h"


SCGraphicsController::SCGraphicsController() : color(100, 130, 255, 180) {
    insertedItem = NULL;
    scene = new SCScene();
    connectWithScene();
}


SCGraphicsController::~SCGraphicsController() {
    if (scene) delete scene;
}


/*
  Returns scene.
*/
SCScene* SCGraphicsController::getScene() {
    return scene;
}


/*
  Save scene to file.
*/
void SCGraphicsController::save(QString filename) {
    SCSceneSaver actionSave;
    actionSave.setTargetFile(filename);
    actionSave.setScene(scene);
    actionSave.save();
}


/*
  Load scene from file.
*/
void SCGraphicsController::load(QString filename) {
    SCSceneLoader actionLoad;
    actionLoad.setFile(filename);
    if (scene) delete scene;
    scene = actionLoad.load();
    connectWithScene();
}


/*
  Sets selection color.
*/
void SCGraphicsController::setSelectionColor(QColor color) {
    this->color = color;
    scene->setSelectionColor(color);
}


/*
  Sets selection border color.
*/
void SCGraphicsController::setSelectionBorderColor(QColor color) {
    scene->setSelectionBorderColor(color);
}


/*
  Inserts a rectangle.
*/
void SCGraphicsController::insertRectangle() {
    SCRectangleItem *rectangle = new SCRectangleItem();
    SCResizableItem *item = new SCResizableItem(rectangle);
    rectangle->setAreaBrush(QBrush(color));
    insertItem(item);
}


/*
  Inserts ellipse.
*/
void SCGraphicsController::insertEllipse() {
    SCEllipseItem *ellipse = new SCEllipseItem();
    SCResizableItem *item = new SCResizableItem(ellipse);
    ellipse->setAreaBrush(color);
    insertItem(item);
}


/*
  Inserts Multi Item.
*/
void SCGraphicsController::insertMultiItem() {

}


/*
  Inserts a group with selected items.
*/
void SCGraphicsController::insertGroup() {
    QList<QGraphicsItem*> si = scene->selectedItems();
    if (si.count() <= 1) return;
    SCItemGroup *itemGroup = new SCItemGroup();

    foreach (QGraphicsItem *outerItem, si) {
        if (0 == outerItem->parentItem()) {
            itemGroup->addToGroup(outerItem);
        }
    }

    SCResizableItem *ri = new SCResizableItem(itemGroup);
    scene->addItem(ri);
}


/*
  Inserts line.
*/
void SCGraphicsController::insertLine() {
    SCLineItem *line = new SCLineItem();
    SCResizableItem *ri = new SCResizableItem(line);
    insertItem(ri);
}


/*
  Sends selection to back.
*/
void SCGraphicsController::sendSelectionToBack() {
    scene->sendSelectionToBack();
}


/*
  Brings selection to front.
*/
void SCGraphicsController::bringSelectionToFront() {
    scene->bringSelectionToFront();
}


/*
  Rotates selection at given angle.
*/
void SCGraphicsController::rotateSelection(qreal angle) {
    scene->rotateSelection(angle);
}


/*
  Sets data for selected item.
*/
void SCGraphicsController::setSelectionData(QMap<int, QString> data) {
    if (!isOnlyOneTopItemInSelection()) return;

    QList<QGraphicsItem*> si = scene->selectedItems();

    QBrush brush;
    QPen pen;
    QColor color;

    foreach (QGraphicsItem *item, si) {
        if (NULL == item->parentItem()) {
            item->setData(Name, data.value(Name));
            brush = dynamic_cast<SCItem*>(item)->getAreaBrush();
            color = brush.color();
            color.setAlpha(data.value(Alpha).toInt());
            brush.setColor(color);
            dynamic_cast<SCItem*>(item)->setAreaBrush(brush);
            pen = dynamic_cast<SCItem*>(item)->getPen();
            pen.setWidth(data.value(BorderSize).toFloat());
            dynamic_cast<SCItem*>(item)->setPen(pen);
        }
    }
}


/*
  Gets the data for selection.
*/
void SCGraphicsController::getSelectionData() {
    QList<QGraphicsItem*> si = scene->selectedItems();
    QMap<int, QString> data;

    if (!isOnlyOneTopItemInSelection()) {
        emit selectionDataChanged(data);
        return;
    }

    foreach (QGraphicsItem* item, si) {
        if (NULL == item->parentItem()) {
            data.insert(Name, item->data(Name).toString());
            data.insert(Alpha, QString::number(dynamic_cast<SCItem*>(item)->getAreaBrush().color().alpha()));
            data.insert(BorderSize, QString::number(dynamic_cast<SCItem*>(item)->getPen().width()));
        }
    }


    emit selectionDataChanged(data);
}


/*
  Copy selected items.
*/
void SCGraphicsController::copySelection() {
    foreach (QGraphicsItem *item, copied) {
        if (NULL == item->scene()) {
            delete item;
        }
    }

    copied.erase(copied.begin(), copied.end());

    QList<QGraphicsItem*> si = scene->selectedItems();

    foreach (QGraphicsItem *item, si) {
        if (NULL == item->parentItem()) {
            SCClonable *clonedItem = dynamic_cast<SCClonable*>(item)->clone();
            copied.append(dynamic_cast<QGraphicsItem*>(clonedItem));
        }
    }
}


/*
  Paste selected items.
*/
void SCGraphicsController::pasteSelection() {
    foreach (QGraphicsItem *item, copied) {
        if (dynamic_cast<SCResizableItem*>(item)) {
            scene->addItem(dynamic_cast<SCResizableItem*>(item));
        }
    }
}


/*
  Removes items.
*/
void SCGraphicsController::removeSelection() {
    foreach (QGraphicsItem *item, scene->selectedItems()) {
        scene->removeItem(item);
    }
    scene->invalidate();
}


/*
  Remove a group.
*/
void SCGraphicsController::removeGroup() {
    QList<QGraphicsItem*> s = scene->selectedItems();
    if (1 != s.count()) return;
    SCResizableItem *ri = dynamic_cast<SCResizableItem*>(s.first());
    if (SCItemGroup::Type != ri->getItem()->type()) return;

    scene->clearSelection();

    QList<QGraphicsItem*> t = ri->getItem()->children();

    QTransform riTransform;
    QPointF top;

    foreach (QGraphicsItem *item, t) {
        if (ri->getItem() == item->parentItem()) {
            top = item->scenePos();
            item->setPos(top);
            riTransform = ri->getItem()->transform();
            riTransform = QTransform(riTransform.m11(), riTransform.m12(), riTransform.m13(), riTransform.m21(), riTransform.m22(), riTransform.m23(), 0, 0, riTransform.m33());
            //dynamic_cast<SCResizableItem*>(item)->setTransformToInner(riTransform, true);
            //item->setParentItem(0);
            item->setTransform(riTransform, true);
            item->setSelected(false);
            item->setParentItem(ri->parentItem());
            if (SCResizableItem::Type == item->type()) {
                dynamic_cast<SCResizableItem*>(item)->setTransformToInner(item->transform(), true);
                item->setTransform(QTransform());
            }
        }
    }

    scene->removeItem(ri);
}


/*
  Sets a connection between scene and object.
*/
void SCGraphicsController::connectWithScene() {
    connect(scene, SIGNAL(selectionChanged()), this, SLOT(getSelectionData()));
}


/*
  Sets scene mode.
*/
void SCGraphicsController::setSceneMode(SCScene::SceneMode mode) {
    scene->setMode(mode);
}


/*
  Checks if the inserted item is added to scene.
*/
bool SCGraphicsController::isInsertedItemAddedToScene() {
    if (NULL == insertedItem) return true;
    return NULL != insertedItem->scene();
}


/*
  Checks if there is only one top item in the selection.
*/
bool SCGraphicsController::isOnlyOneTopItemInSelection() {
    QList<QGraphicsItem*> si = scene->selectedItems();
    int count = 0;
    foreach (QGraphicsItem *item, si) {
        if (NULL == item->parentItem()) {
            count++;
            if (2 <= count) return false;
        }
    }
    if (0 == count) return false;
    return true;
}


/*
  Inserts item.
*/
void SCGraphicsController::insertItem(SCResizableItem *item) {
    setSceneMode(SCScene::Insertion);
    item->setData(Name, "Default");
    if (!isInsertedItemAddedToScene()) delete insertedItem;
    insertedItem = item;
    scene->setInserted(insertedItem);
}
