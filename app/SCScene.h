#ifndef SCSCENE_H
#define SCSCENE_H

#include <QGraphicsScene>

class SCGraphicsController;
class SCResizableItem;

class SCScene : public QGraphicsScene {
    Q_OBJECT

public:
    enum SceneMode {Selection, Insertion};
    SCScene();
    void setMode(SCScene::SceneMode);
    void setInserted(SCResizableItem*);
    void addItem(SCResizableItem*);
    void addItemKeepZ(SCResizableItem*);
    void sendSelectionToBack();
    void bringSelectionToFront();
    void setSelectionColor(QColor);
    void setSelectionBorderColor(QColor);
    void rotateSelection(qreal);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent*);
    void mouseMoveEvent(QGraphicsSceneMouseEvent*);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent*);

private:
    SceneMode mode;
    SCResizableItem *inserted;
    QPointF startPos;
    qreal maxZ;
    qreal minZ;
};

#endif // SCSCENE_H
