#ifndef SCSCENESAVER_H
#define SCSCENESAVER_H

#include <QString>

class SCScene;
class QColor;
class QPointF;
class QXmlStreamWriter;
class QSizeF;
class QTransform;
class QGraphicsItem;

class SCSceneSaver {

public:
    void setTargetFile(QString);
    void setScene(SCScene*);
    void save() const;

private:
    SCScene *scene;
    QString fileName;
};

#endif // SCSCENESAVER_H
