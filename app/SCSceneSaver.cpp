#include "SCSceneSaver.h"
#include <QtAlgorithms>
#include <QXmlStreamWriter>
#include <QFile>
#include "SCScene.h"
#include <QGraphicsItem>
#include "SCGraphicsController.h"
#include "items/SCResizableItem.h"
#include "items/SCRectangleItem.h"
#include "items/SCItemGroup.h"


void SCSceneSaver::setTargetFile(QString file) {
    this->fileName = file;
}


void SCSceneSaver::setScene(SCScene *scene) {
    this->scene = scene;
}


void SCSceneSaver::save() const {
    QFile file(fileName);

    if (!file.open(QFile::WriteOnly | QFile::Text)) return;

    QList<QGraphicsItem*> items = scene->items();

    qreal exportNumber = 0;

    QXmlStreamWriter xml;
    xml.setAutoFormatting(true);
    xml.setDevice(&file);
    xml.writeStartDocument();
    xml.writeDTD("<!DOCTYPE scene>");
    xml.writeStartElement("scene");

    foreach (QGraphicsItem *outerItem, items) {
        if (SCResizableItem::Type == outerItem->type()) {
            dynamic_cast<SCExportable*>(outerItem)->exportt(&xml, exportNumber);
            exportNumber++;
        }
    }

    xml.writeEndElement();
    xml.writeEndDocument();
    file.close();
}
