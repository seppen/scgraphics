#include "SCScene.h"
#include "SCGraphicsController.h"
#include <cmath>
#include <QList>
#include "items/SCResizableItem.h"
#include "items/SCItemGroup.h"
#include "items/SCLineItem.h"
#include <QGraphicsSceneMouseEvent>


SCScene::SCScene() {
    minZ = 0;
    maxZ = 0;
}


/*
  Set scene mode;
*/
void SCScene::setMode(SceneMode mode) {
    this->mode = mode;
}


/*
  Adding reference to item that is being inserted.
*/
void SCScene::setInserted(SCResizableItem *item) {
    inserted = item;
}


/*
  Add item to scene.
*/
void SCScene::addItem(SCResizableItem *item) {
    maxZ += 0.1;
    item->setZValue(maxZ);
    QGraphicsScene::addItem(item);
}


/*
  Adds item and keeps its z value.
*/
void SCScene::addItemKeepZ(SCResizableItem *item) {
    maxZ = std::max(maxZ, item->zValue());
    minZ = std::min(minZ, item->zValue());
    QGraphicsScene::addItem(item);
}


/*
  Sends items in selection to back.
*/
void SCScene::sendSelectionToBack() {
    QList<QGraphicsItem*> si = selectedItems();
    if (si.isEmpty()) return;
    QGraphicsItem *max = *si.begin();
    while (!si.isEmpty()) {
        for (QList<QGraphicsItem*>::iterator it = si.begin(); it != si.end(); it++) {
            max = (max->zValue() >= (*it)->zValue()) ? max : *it;
        }
        minZ -= 0.1;
        max->setZValue(minZ);
        si.removeAll(max);
    }
}


/*
  Brings items in selection to front.
*/
void SCScene::bringSelectionToFront() {
    QList<QGraphicsItem*> si = selectedItems();
    if (si.isEmpty()) return;
    QGraphicsItem *min = *si.begin();
    while (!si.isEmpty()) {
        for (QList<QGraphicsItem*>::iterator it = si.begin(); it != si.end(); it++) {
            min = (min->zValue() <= (*it)->zValue()) ? min : *it;
        }
        maxZ += 0.1;
        min->setZValue(maxZ);
        si.removeAll(min);
    }
}


/*
  Sets the color of the items in the selection.
*/
void SCScene::setSelectionColor(QColor color) {
    QGraphicsItem *i = NULL;
    foreach (i, selectedItems()) {
        dynamic_cast<SCResizableItem*>(i)->setAreaBrush(QBrush(color));
    }
}


/*
  Sets selection border color.
*/
void SCScene::setSelectionBorderColor(QColor color) {
    QGraphicsItem *i = NULL;
    foreach (i, selectedItems()) {
        SCResizableItem *item = dynamic_cast<SCResizableItem*>(i);
        QPen pen = item->getPen();
        pen.setColor(color);
        item->setPen(pen);
    }
}


/*
  Rotates items in the selection.
*/
void SCScene::rotateSelection(qreal angle) {
    QList<QGraphicsItem*> si = selectedItems();
    foreach (QGraphicsItem *item, si) {
        dynamic_cast<SCResizableItem*>(item)->rotateItem(angle);
    }
}


void SCScene::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    switch (mode) {
    case Insertion:
        inserted->setPos(event->scenePos());
        startPos = event->scenePos();
        addItem(inserted);
        event->accept();
        break;
    default:
        QGraphicsScene::mousePressEvent(event);
    }
}


void SCScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    QPointF top;
    QRectF v;
    switch (mode) {
    case Insertion:
        top = QPointF(
                    std::min(startPos.x(), event->scenePos().x()),
                    std::min(startPos.y(), event->scenePos().y())
        );
        inserted->setPos(top);
        inserted->resizeToRectangle(startPos, event->scenePos());
        invalidate();
        event->accept();
        break;
    default:
        QGraphicsScene::mouseMoveEvent(event);
    }
}


void SCScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    switch (mode) {
    case Insertion:
        mode = Selection;
        inserted = NULL;
        break;
    default:
        QGraphicsScene::mouseReleaseEvent(event);
    }
}
