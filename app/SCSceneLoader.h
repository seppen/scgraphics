#ifndef SCSCENELOADER_H
#define SCSCENELOADER_H

#include <QString>
#include "SCScene.h"

class QXmlStreamReader;

class SCSceneLoader {

public:
    void setFile(QString);
    SCScene* load() const;

private:
    QString fileName;
};

#endif // SCSCENELOADER_H
