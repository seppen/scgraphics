#include "IO.h"
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QPointF>
#include <QColor>
#include <QTransform>
#include <QSize>
#include <QPen>


void IO::writePen(QXmlStreamWriter *xml, QPen pen) {
    xml->writeStartElement("pen");
    xml->writeAttribute("size", QString::number(pen.width()));
    IO::writeColor(xml, pen.color());
    xml->writeEndElement();
}


void IO::writePos(QXmlStreamWriter *xml, QPointF pos) {
    xml->writeStartElement("pos");
    xml->writeAttribute("x", QString::number(pos.x()));
    xml->writeAttribute("y", QString::number(pos.y()));
    xml->writeEndElement();
}


void IO::writeColor(QXmlStreamWriter *xml, QColor color) {
    xml->writeStartElement("color");
    xml->writeAttribute("red", QString::number(color.red()));
    xml->writeAttribute("green", QString::number(color.green()));
    xml->writeAttribute("blue", QString::number(color.blue()));
    xml->writeAttribute("aplha", QString::number(color.alpha()));
    xml->writeEndElement();
}


void IO::writeSize(QXmlStreamWriter *xml, QSizeF size) {
    xml->writeStartElement("size");
    xml->writeAttribute("width", QString::number(size.width()));
    xml->writeAttribute("height", QString::number(size.height()));
    xml->writeEndElement();
}


void IO::writeTransform(QXmlStreamWriter *xml, QTransform transform) {
    xml->writeStartElement("transform");
    xml->writeAttribute("m11", QString::number(transform.m11(), 'g', 10));
    xml->writeAttribute("m12", QString::number(transform.m12(), 'g', 10));
    xml->writeAttribute("m13", QString::number(transform.m13(), 'g', 10));
    xml->writeAttribute("m21", QString::number(transform.m21(), 'g', 10));
    xml->writeAttribute("m22", QString::number(transform.m22(), 'g', 10));
    xml->writeAttribute("m23", QString::number(transform.m23(), 'g', 10));
    xml->writeAttribute("m31", QString::number(transform.m31(), 'g', 10));
    xml->writeAttribute("m32", QString::number(transform.m32(), 'g', 10));
    xml->writeAttribute("m33", QString::number(transform.m33(), 'g', 10));
    xml->writeEndElement();
}


QPen IO::readNextPen(QXmlStreamReader *xml) {
    QPen pen;
    IO::jumpToNextElement(xml, "pen");
    pen.setWidth(xml->attributes().value("size").toString().toDouble());
    pen.setColor(IO::readNextColor(xml));
    return pen;
}


QPointF IO::readNextPos(QXmlStreamReader *xml) {
    IO::jumpToNextElement(xml, "pos");
    qreal x = xml->attributes().value("x").toString().toDouble();
    qreal y = xml->attributes().value("y").toString().toDouble();
    return QPointF(x, y);
}


QColor IO::readNextColor(QXmlStreamReader *xml) {
    IO::jumpToNextElement(xml, "color");
    int red = xml->attributes().value("red").toString().toInt();
    int green = xml->attributes().value("green").toString().toInt();
    int blue = xml->attributes().value("blue").toString().toInt();
    int aplha = xml->attributes().value("aplha").toString().toInt();
    return QColor(red, green, blue, aplha);
}


QSizeF IO::readNextSize(QXmlStreamReader *xml) {
    IO::jumpToNextElement(xml, "size");
    qreal width = xml->attributes().value("width").toString().toDouble();
    qreal height = xml->attributes().value("height").toString().toDouble();
    return QSizeF(width, height);
}


QTransform IO::readNextTransform(QXmlStreamReader *xml) {
    IO::jumpToNextElement(xml, "transform");
    qreal m11 = xml->attributes().value("m11").toString().toDouble();
    qreal m12 = xml->attributes().value("m12").toString().toDouble();
    qreal m13 = xml->attributes().value("m13").toString().toDouble();
    qreal m21 = xml->attributes().value("m21").toString().toDouble();
    qreal m22 = xml->attributes().value("m22").toString().toDouble();
    qreal m23 = xml->attributes().value("m23").toString().toDouble();
    qreal m31 = xml->attributes().value("m31").toString().toDouble();
    qreal m32 = xml->attributes().value("m32").toString().toDouble();
    qreal m33 = xml->attributes().value("m33").toString().toDouble();
    return QTransform(m11, m12, m13, m21, m22, m23, m31, m32, m33);
}


void IO::jumpToNextElement(QXmlStreamReader *xml, QString name) {
    while ((name != xml->name().toString() || !xml->isStartElement()) && !xml->atEnd()) {
        xml->readNext();
    }
    if (xml->atEnd()) qWarning("not valid");
}
