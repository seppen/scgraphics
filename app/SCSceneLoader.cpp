#include "SCSceneLoader.h"
#include <QXmlStreamReader>
#include <QtXmlPatterns/QXmlQuery>
#include <QFile>
#include <iostream>
#include "items/SCRectangleItem.h"
#include "items/SCEllipseItem.h"
#include "items/SCResizableItem.h"
#include "items/SCItemGroup.h"
#include "IO.h"


void SCSceneLoader::setFile(QString file) {
    this->fileName = file;
}


SCScene* SCSceneLoader::load() const {
    QFile file(fileName);

    if (!file.open(QFile::ReadOnly | QFile::Text)) return 0;

    SCScene *scene = new SCScene();
    QMap<qreal, QGraphicsItem*> imported;
    QXmlStreamReader xml;
    xml.setDevice(&file);
    while (!xml.atEnd()) {
        IO::jumpToNextElement(&xml, SCResizableItem::tag());
        if (SCResizableItem::tag() == xml.name().toString() && xml.isStartElement()) {
            SCResizableItem *ri = SCResizableItem::import(&xml, imported);
            scene->addItemKeepZ(ri);
            imported.insert(ri->getId(), ri);
        }
    }
    file.close();
    return scene;
}
