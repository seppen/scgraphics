#include <QtGui/QApplication>
#include "gui/SCMainWindow.h"

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    SCMainWindow w;
    w.show();

    return a.exec();
}
